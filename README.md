# averagedatetag

Calculates the average date tag of a music library.

## How to use
Just run `./averagedatetag.py path/to/my/library`. In the working directory the file `plot.pdf` and `plot.png` get created.

## Dependencies
matplotlib, mutagen

## Supported file types
.mp3, .flac, .ogg, .opus, .m4a

## Supported tags
originaldate, originalyear, date, TXXX:originalyear, TDOR, TDRC, ©day

