#!/usr/bin/env python3
import sys
import os
from pathlib import Path
from datetime import date, timedelta
import mutagen
import matplotlib.pyplot as plt

def getfiles(top):
    for root, dirs, files in os.walk(top):
        for file in files:
            yield Path(root).joinpath(file)

def main():
    startpath = Path(sys.argv[1])

    dates = { 'None': 0 }

    relevant_extension = ('.mp3', '.flac', '.ogg', '.opus', '.m4a')
    unrecognised = []
    for file in getfiles(startpath):
        if file.suffix not in relevant_extension:
            if file.suffix not in unrecognised:
                print('Extension {} of file {} not in list.'.format(file.suffix, file))
                unrecognised.append(file.suffix)
            continue

        date_str = None
        tags = mutagen.File(file)
        keys = ('originaldate', 'originalyear', 'date', 'TXXX:originalyear', 'TDOR', 'TDRC', '©day')
        for key in keys:
            if key in tags.keys():
                date_str = tags[key]
                if '[' in str(date_str):
                    date_str = date_str[0]

        if not date_str or not str(date_str):
            print('Unclear date in {}.'.format(file))
            dates['None'] += 1
            continue

        date_str = str(date_str)

        if date_str not in dates:
            dates[date_str] = 0
        dates[date_str] += 1

    print('Dates with numbers:', dates)
    print('Dates only:', sorted(dates.keys()))

    # calc mean
    weightedsum = 0
    valuesum = 0
    unixzero = date(1970, 1, 1)
    for key in dates:
        if key == 'None':
            continue

        key_date = None
        uncertainty = None
        if len(key) == 4:
            key_date = date(int(key), 7, 1)
        elif len(key) == 8:
            key_date = date(int(key[:4]), int(key[4:6]), int(key[6:]))
        elif len(key) == 10 and key.endswith("-00-00"):
            key_date = date(int(key[:4]), 7, 1)
        elif len(key) == 10:
            key_date = date.fromisoformat(key)
        elif len(key) == 7:
            key_date = date(int(key[:4]), int(key[5:]), 15)
        else:
            try:
                key_date = date.fromisoformat(key[:10])
            except:
                print('Cannot read date', key)
                print(key[:10])
                return

        unixseconds = (key_date - unixzero).total_seconds()
        weightedsum += unixseconds * dates[key]
        valuesum += dates[key]
    mean = weightedsum / valuesum
    meandate = unixzero + timedelta(seconds=mean)
    print('Mean date:', meandate)

    years = []
    counts = []
    for key in sorted(dates):
        if key == 'None':
            continue
        years.append(int(key[:4]))
        counts.append(dates[key])

    fig, ax = plt.subplots()
    ax.bar(years, counts, width=1, align='edge', label='Number of songs for each year')
    ax.bar(min(years) - 1, dates['None'], align='edge', color='red', label='No date in file')
    mean_year = meandate.year + (meandate - date(meandate.year, 1, 1)).total_seconds() / (365.2425 * 86400)
    ax.plot([mean_year, mean_year], [0, max(counts)], label='Mean date: {}'.format(meandate), color='orange')
    ax.legend()
    ax.grid()
    ax.set_xlabel('Year')
    ax.set_ylabel('Counts')
    ax.set_title('Release Years in my Music Library')
    fig.savefig('./plot.pdf')
    fig.savefig('./plot.png', dpi=508) # 200 dots/cm

if __name__ == "__main__":
    main()
